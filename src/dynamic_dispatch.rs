use std::fmt::Debug;

pub trait Operand: Debug {
    fn get(&self) -> i64;
}

impl Operand for i64 {
    fn get(&self) -> i64 {
        *self
    }
}

#[derive(Debug)]
pub struct AddOperator(pub Box<dyn Operand>, pub Box<dyn Operand>);

impl Operand for AddOperator {
    fn get(&self) -> i64 {
        self.0.get() + self.1.get()
    }
}

#[derive(Debug)]
pub struct SubOperator(pub Box<dyn Operand>, pub Box<dyn Operand>);

impl Operand for SubOperator {
    fn get(&self) -> i64 {
        self.0.get() - self.1.get()
    }
}

#[derive(Debug)]
pub struct MulOperator(pub Box<dyn Operand>, pub Box<dyn Operand>);

impl Operand for MulOperator {
    fn get(&self) -> i64 {
        self.0.get() * self.1.get()
    }
}

#[derive(Debug)]
pub struct DivOperator(pub Box<dyn Operand>, pub Box<dyn Operand>);

impl Operand for DivOperator {
    fn get(&self) -> i64 {
        self.0.get() / self.1.get()
    }
}
