use std::fmt::Debug;

pub trait Operand: Copy + Debug {
    fn get(&self) -> i64;
}

impl Operand for i64 {
    fn get(&self) -> i64 {
        *self
    }
}

#[derive(Copy, Clone, Debug)]
pub struct AddOperator<L: Operand, R: Operand>(pub L, pub R);

impl<L: Operand, R: Operand> Operand for AddOperator<L, R> {
    fn get(&self) -> i64 {
        self.0.get() + self.1.get()
    }
}

#[derive(Copy, Clone, Debug)]
pub struct SubOperator<L: Operand, R: Operand>(pub L, pub R);

impl<L: Operand, R: Operand> Operand for SubOperator<L, R> {
    fn get(&self) -> i64 {
        self.0.get() - self.1.get()
    }
}

#[derive(Copy, Clone, Debug)]
pub struct MulOperator<L: Operand, R: Operand>(pub L, pub R);

impl<L: Operand, R: Operand> Operand for MulOperator<L, R> {
    fn get(&self) -> i64 {
        self.0.get() * self.1.get()
    }
}

#[derive(Copy, Clone, Debug)]
pub struct DivOperator<L: Operand, R: Operand>(pub L, pub R);

impl<L: Operand, R: Operand> Operand for DivOperator<L, R> {
    fn get(&self) -> i64 {
        self.0.get() / self.1.get()
    }
}
