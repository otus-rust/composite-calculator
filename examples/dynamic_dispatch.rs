use composite_calculator::dynamic_dispatch::{
    AddOperator, DivOperator, MulOperator, Operand, SubOperator,
};

fn main() {
    let a = AddOperator(Box::new(12), Box::new(34));
    dbg!(&a);
    let b = SubOperator(Box::new(a), Box::new(15));
    dbg!(&b);
    let c = MulOperator(Box::new(3), Box::new(b));
    dbg!(&c);
    let d = DivOperator(Box::new(c), Box::new(31));
    dbg!(&d);

    println!("Result: {}", d.get());
}
