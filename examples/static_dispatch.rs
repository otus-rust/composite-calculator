use composite_calculator::static_dispatch::{
    AddOperator, DivOperator, MulOperator, Operand, SubOperator,
};

fn main() {
    let a = AddOperator(12, 34);
    dbg!(&a);
    let b = SubOperator(a, 15);
    dbg!(&b);
    let c = MulOperator(3, b);
    dbg!(&c);
    let d = DivOperator(c, b);
    dbg!(&d);

    println!("Result: {}", d.get());
}